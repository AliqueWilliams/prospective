import UIKit

protocol LoggedOutInteractorInput {
  func viewDidLoad()
  func facebookLogin(request: LoggedOut.Request.Login)
}

protocol LoggedOutInteractorOutput {
  func present(response: LoggedOut.Response)
}

class LoggedOutInteractor: LoggedOutInteractorInput {
  var output: LoggedOutInteractorOutput!
  var facebookLogin = FacebookLogin()
  
  init() {
    facebookLogin.delegate = self
  }
  
  func viewDidLoad() {
    let response:LoggedOut.Response = {
      var isReachable:LoggedOut.Response
      
      switch Reachability.Status.determine() {
      case .reachable:
        isReachable = LoggedOut.Response(status: .reachable)
      case .unreachable:
        isReachable = LoggedOut.Response(status: .unreachable)
      }
      return isReachable
    }()
    output.present(response: response)
  }
  
  func facebookLogin(request: LoggedOut.Request.Login) {
    facebookLogin.login(with: request.viewController)
  }
}

// MARK: - Facebook Login Delegates
extension LoggedOutInteractor: FacebookLoginDelegate {
  func login(success: Bool) {
    let status: LoggedOut.Response.Status = success ? .loggedIn : .cancelled
    let response = LoggedOut.Response(status: status)
    self.output.present(response: response)
  }
  
  func login(error: Error) {
    let status: LoggedOut.Response.Status = .error(error)
    let response = LoggedOut.Response(status: status)
    self.output.present(response: response)
  }
  
  func willLogIn() {
    let response = LoggedOut.Response(status: .loggingIn)
    self.output.present(response: response)
  }
}
