import UIKit

protocol LoggedOutViewControllerInput {
  func display(viewModel: LoggedOut.ViewModel)
}

protocol LoggedOutViewControllerOutput {
  func viewDidLoad()
  func facebookLogin(request: LoggedOut.Request.Login)
}

class LoggedOutViewController: UIViewController, LoggedOutViewControllerInput {
  @IBOutlet weak var usageDemonstration: UIScrollView!
  @IBOutlet weak var pageIndicator: UIPageControl!
  @IBOutlet weak var loggingIn: UIActivityIndicatorView!
  @IBOutlet weak var facebookLogin: UIButton!
  
  var output: LoggedOutViewControllerOutput!
  var router: LoggedOutRouter!
  
  let demoImages: [UIImage] = {
    var images: [UIImage] = []
    for index in (1...3) {
      guard let image = UIImage(named:"IVF-\(index)") else { continue }
      images.append(image)
    }
    return images
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    LoggedOutConfigurator.configure(viewController: self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    output.viewDidLoad()
    configureDemoImages()
  }
  
  @IBAction func facebookLoginWasTapped(_ sender: UIButton) {
    let request = LoggedOut.Request.Login(viewController: self)
    output.facebookLogin(request: request)
  }
}

// MARK: - Image and scroll view setup
extension LoggedOutViewController: UIScrollViewDelegate {
  func configureDemoImages() {
    usageDemonstration.contentSize = CGSize(width: view.bounds.width * CGFloat(demoImages.count), height: 582)
    usageDemonstration.delegate = self
    
    for (index, image) in demoImages.enumerated() {
      if let imageContainer = Bundle.main.loadNibNamed("LoggedOutImageContainer", owner: self, options: nil)?.first as? LoggedOutImageContainer {
        imageContainer.container.image = image
        usageDemonstration.addSubview(imageContainer)
        imageContainer.frame.size.width = view.frame.size.width
        imageContainer.frame.origin.x = view.bounds.size.width * CGFloat(index)
      }
    }
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let page = scrollView.contentOffset.x / scrollView.frame.size.width
    pageIndicator.currentPage = Int(page)
  }
}

// MARK: - View Model handling
extension LoggedOutViewController {
  func display(viewModel: LoggedOut.ViewModel) {
    usageDemonstration.isHidden = viewModel.usageDemonstrationIsHidden
    pageIndicator.isHidden = viewModel.pageIndicatorIsHidden
    facebookLogin.isHidden = viewModel.facebookLoginIsHidden
    facebookLogin.titleLabel?.layer.opacity = viewModel.facebookLoginTextIsHidden ? 0.0 : 1.0
    
    if viewModel.loggingInIsSpinning {
      loggingIn.startAnimating()
    } else {
      loggingIn.stopAnimating()
    }

    if viewModel.loggedIn {
      router.navigate(to: .termsOfServices)
    }
  }
}
