import UIKit
import FBSDKLoginKit
import FirebaseAuth

protocol FacebookLoginDelegate {
  func login(success: Bool)
  func login(error: Error)
  func willLogIn()
}

class FacebookLogin {
  var delegate: FacebookLoginDelegate?
  
  func login(with viewController: UIViewController) {
    FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: viewController) { result, error in
      guard error == nil else {
        self.delegate?.login(error: error!)
        return
      }
      
      if (result?.isCancelled)! == true {
        self.delegate?.login(success: false)
        return
      }
      
      self.delegate?.willLogIn()
      OperationQueue().addOperation {
        self.authenticateWithFirebase()
      }
    }
  }
  
  func authenticateWithFirebase() {
    guard let token = FBSDKAccessToken.current().tokenString else { return }
    let credential = FIRFacebookAuthProvider.credential(withAccessToken: token)
    FIRAuth.auth()?.signIn(with: credential) { user, error in
      guard error == nil else {
        self.delegate?.login(error: error!)
        return
      }
      self.processResult()
    }
  }
  
  func processResult() {
    let permissions = ["fields": "id, first_name, last_name, gender, picture.type(large), email"]
    FBSDKGraphRequest(graphPath: "/me", parameters: permissions).start { connection, result, error in
      guard error == nil else { return }
      guard let json = result as? [String: AnyObject] else { return }
      let user = User(json: json) // TODO: Save user
      OperationQueue.main.addOperation {
        self.delegate?.login(success: true)
      }
    }
  }
}
