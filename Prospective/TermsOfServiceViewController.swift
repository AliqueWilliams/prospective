import UIKit

protocol TermsOfServiceViewControllerInput {
  func displaySomething(viewModel: TermsOfService.Something.ViewModel)
}

protocol TermsOfServiceViewControllerOutput {
  func doSomething(request: TermsOfService.Something.Request)
}

class TermsOfServiceViewController: UIViewController, TermsOfServiceViewControllerInput {
  var output: TermsOfServiceViewControllerOutput!
  var router: TermsOfServiceRouter!
  
  // MARK: - Object lifecycle
  
  override func awakeFromNib() {
    super.awakeFromNib()
    TermsOfServiceConfigurator.sharedInstance.configure(viewController: self)
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    doSomethingOnLoad()
  }
  
  // MARK: - Event handling
  
  func doSomethingOnLoad() {
    // NOTE: Ask the Interactor to do some work
    
    let request = TermsOfService.Something.Request()
    output.doSomething(request: request)
  }
  
  // MARK: - Display logic
  
  func displaySomething(viewModel: TermsOfService.Something.ViewModel) {
    // NOTE: Display the result from the Presenter
    
    // nameTextField.text = viewModel.name
  }
}
