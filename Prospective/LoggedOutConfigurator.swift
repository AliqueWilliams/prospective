import UIKit

extension LoggedOutViewController: LoggedOutPresenterOutput {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    router.passDataToNextScene(segue: segue)
  }
}

extension LoggedOutInteractor: LoggedOutViewControllerOutput {}

extension LoggedOutPresenter: LoggedOutInteractorOutput {}

class LoggedOutConfigurator {
  private init() {}
  
  static func configure(viewController: LoggedOutViewController) {
    let router = LoggedOutRouter()
    router.viewController = viewController
    
    let presenter = LoggedOutPresenter()
    presenter.output = viewController
    
    let interactor = LoggedOutInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
