import Foundation
import FirebaseDatabase

final class FirebaseDatabase {
  var databaseReference: FIRDatabaseReference!
  
  init() {
    databaseReference = FIRDatabase.database().reference()
  }
}
