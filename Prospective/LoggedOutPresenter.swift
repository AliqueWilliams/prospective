import UIKit

protocol LoggedOutPresenterInput {
  func present(response: LoggedOut.Response)
}

protocol LoggedOutPresenterOutput: class {
  func display(viewModel: LoggedOut.ViewModel)
}

class LoggedOutPresenter: LoggedOutPresenterInput {
  weak var output: LoggedOutPresenterOutput!

  func present(response: LoggedOut.Response) {
    var viewModel = LoggedOut.ViewModel()
    
    switch response.status {
    case .reachable, .cancelled:
      break
    case .unreachable:
      viewModel.pageIndicatorIsHidden = true
      viewModel.facebookLoginIsHidden = true
      viewModel.usageDemonstrationIsHidden = true
    case .loggingIn:
      viewModel.facebookLoginTextIsHidden = true
      viewModel.loggingInIsSpinning = true
    case .loggedIn:
      viewModel.loggedIn = true
    case .error(let error):
      viewModel.errorMessage = error.localizedDescription
    }
    output.display(viewModel: viewModel)
  }
}
