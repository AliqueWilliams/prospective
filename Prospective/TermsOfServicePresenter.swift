import UIKit

protocol TermsOfServicePresenterInput {
  func presentSomething(response: TermsOfService.Something.Response)
}

protocol TermsOfServicePresenterOutput: class {
  func displaySomething(viewModel: TermsOfService.Something.ViewModel)
}

class TermsOfServicePresenter: TermsOfServicePresenterInput {
  weak var output: TermsOfServicePresenterOutput!
  
  // MARK: - Presentation logic
  
  func presentSomething(response: TermsOfService.Something.Response) {
    // NOTE: Format the response from the Interactor and pass the result back to the View Controller
    
    let viewModel = TermsOfService.Something.ViewModel()
    output.displaySomething(viewModel: viewModel)
  }
}
