import UIKit

protocol TermsOfServiceInteractorInput {
  func doSomething(request: TermsOfService.Something.Request)
}

protocol TermsOfServiceInteractorOutput {
  func presentSomething(response: TermsOfService.Something.Response)
}

class TermsOfServiceInteractor: TermsOfServiceInteractorInput {
  var output: TermsOfServiceInteractorOutput!
  var worker: TermsOfServiceWorker!
  
  // MARK: - Business logic
  
  func doSomething(request: TermsOfService.Something.Request) {
    // NOTE: Create some Worker to do the work
    
    worker = TermsOfServiceWorker()
    worker.doSomeWork()
    
    // NOTE: Pass the result to the Presenter
    
    let response = TermsOfService.Something.Response()
    output.presentSomething(response: response)
  }
}
