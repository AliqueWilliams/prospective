import UIKit

protocol LoggedOutRouterInput {
}

class LoggedOutRouter: LoggedOutRouterInput, SegueHelper {
  weak var viewController: LoggedOutViewController!
  
  enum SegueIdentifier: String {
    case termsOfServices
  }
  
  func navigate(to segue: SegueIdentifier) {
    switch segue {
    case .termsOfServices:
      viewController.performSegue(withIdentifier: SegueIdentifier.termsOfServices.rawValue, sender: nil)
    }
  }
  
  func passDataToNextScene(segue: UIStoryboardSegue) {
    switch segueIdentifier(for: segue) {
    case .termsOfServices:
      passDataToTermsOfServices(segue: segue)
    }
  }
  
  func passDataToTermsOfServices(segue: UIStoryboardSegue) {
    // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
    // someWhereViewController.output.name = viewController.output.name
  }
}
