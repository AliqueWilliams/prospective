import UIKit

// MARK: - Connect View, Interactor, and Presenter

extension TermsOfServiceViewController: TermsOfServicePresenterOutput {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    router.passDataToNextScene(segue: segue)
  }
}

extension TermsOfServiceInteractor: TermsOfServiceViewControllerOutput {
}

extension TermsOfServicePresenter: TermsOfServiceInteractorOutput {
}

class TermsOfServiceConfigurator {
  // MARK: - Object lifecycle
  
  static let sharedInstance = TermsOfServiceConfigurator()
  
  private init() {}
  
  // MARK: - Configuration
  
  func configure(viewController: TermsOfServiceViewController) {
    let router = TermsOfServiceRouter()
    router.viewController = viewController
    
    let presenter = TermsOfServicePresenter()
    presenter.output = viewController
    
    let interactor = TermsOfServiceInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
