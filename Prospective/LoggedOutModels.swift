import UIKit

struct LoggedOut {
  struct ViewModel {
    var usageDemonstrationIsHidden = false
    var pageIndicatorIsHidden = false
    var loggingInIsSpinning = false
    var facebookLoginIsHidden = false
    var facebookLoginTextIsHidden = false
    var errorMessage = ""
    var loggedIn = false
  }
  
  struct Response {
    var status: Status
    enum Status {
      case unreachable
      case reachable
      case loggingIn
      case loggedIn
      case cancelled
      case error(Error)
    }
  }
  
  struct Request {
    struct Login {
      var viewController: UIViewController
    }
  }
}
