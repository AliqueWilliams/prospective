import UIKit

protocol SegueHelper: class {
  associatedtype SegueIdentifier: RawRepresentable
}

extension SegueHelper where SegueIdentifier.RawValue == String {
  func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier {
    guard let identifier = segue.identifier, let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
      fatalError("Couldn't handle segue identifier \(segue.identifier ?? "") for view controller of type \(type(of: self)).")
    }
    return segueIdentifier
  }
}
