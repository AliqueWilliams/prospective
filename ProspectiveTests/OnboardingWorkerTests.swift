@testable import Prospective
import XCTest

class OnboardingWorkerTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: OnboardingWorker!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupOnboardingWorker()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupOnboardingWorker() {
    sut = OnboardingWorker()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
