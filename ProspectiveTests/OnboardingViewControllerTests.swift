@testable import Prospective
import XCTest

class OnboardingViewControllerTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: OnboardingViewController!
  var window: UIWindow!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    window = UIWindow()
    setupOnboardingViewController()
  }
  
  override func tearDown() {
    window = nil
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupOnboardingViewController() {
    let bundle = Bundle.main
    let storyboard = UIStoryboard(name: "Main", bundle: bundle)
    sut = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
  }
  
  func loadView() {
    window.addSubview(sut.view)
    RunLoop.current.run(until: Date())
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
