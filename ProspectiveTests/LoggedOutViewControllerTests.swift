@testable import Prospective
import XCTest

class LoggedOutViewControllerTests: XCTestCase {
  var sut: LoggedOutViewController!
  var window: UIWindow!
    
  override func setUp() {
    super.setUp()
    window = UIWindow()
    setupLoggedOutViewController()
  }
  
  override func tearDown() {
    window = nil
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupLoggedOutViewController() {
    let bundle = Bundle.main
    let storyboard = UIStoryboard(name: "LoggedOut", bundle: bundle)
    sut = storyboard.instantiateViewController(withIdentifier: "LoggedOut") as! LoggedOutViewController
  }
  
  func loadView() {
    window.addSubview(sut.view)
    RunLoop.current.run(until: Date())
  }
  
  func testSomething() {
  }
}
