@testable import Prospective
import XCTest

class OnboardingPresenterTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: OnboardingPresenter!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupOnboardingPresenter()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupOnboardingPresenter() {
    sut = OnboardingPresenter()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
