@testable import Prospective
import XCTest

class LoggedOutWorkerTests: XCTestCase {
  
  var sut: LoggedOutWorker!
    
  override func setUp() {
    super.setUp()
    setupLoggedOutWorker()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupLoggedOutWorker() {
    sut = LoggedOutWorker()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
  }
}
