@testable import Prospective
import XCTest

class LoggedOutInteractorTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: LoggedOutInteractor!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupLoggedOutInteractor()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupLoggedOutInteractor() {
    sut = LoggedOutInteractor()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
