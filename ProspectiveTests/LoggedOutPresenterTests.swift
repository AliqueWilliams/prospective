@testable import Prospective
import XCTest

class LoggedOutPresenterTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: LoggedOutPresenter!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupLoggedOutPresenter()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupLoggedOutPresenter() {
    sut = LoggedOutPresenter()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
