@testable import Prospective
import XCTest

class OnboardingInteractorTests: XCTestCase {
  // MARK: - Subject under test
  
  var sut: OnboardingInteractor!
  
  // MARK: - Test lifecycle
  
  override func setUp() {
    super.setUp()
    setupOnboardingInteractor()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Test setup
  
  func setupOnboardingInteractor() {
    sut = OnboardingInteractor()
  }
  
  // MARK: - Test doubles
  
  // MARK: - Tests
  
  func testSomething() {
    // Given
    
    // When
    
    // Then
  }
}
