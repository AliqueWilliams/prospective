import Foundation

// MARK: - Person Basic Details
struct User {
  typealias UserID = String
  typealias jsonObject = [String: AnyObject]
  
  var id: Int
  var email: String
  var autheticated: Bool = true
  var matches: [UserID]?
  var name: (first: String, last: String)
  var birthday: Date?
  var age: Int? {
    if birthday == nil { return nil }
    return Calendar.current.dateComponents([.year], from: birthday!, to: Date()).year!
  }
  var genderMale: Bool = true
  var orientation: SexualOrientation?
  var status: RelationshipStatus?
  var height: Measurement<UnitLength>?
  var weight: Measurement<UnitMass>?
  var bodyType: TypeOfBody?
  var ethnicity: Ethnicity?
  var diet: Diet?
  var smoking: Smoker?
  var drinking: Drinker?
  var religion: Religion?
  var language = Locale.preferredLanguages.first!
  var summary: UserSummary?
}

// MARK: - Custom Initializers
extension User {
  init(json: jsonObject) {
    id = Int(json["id"] as! String)!
    email = json["email"] as! String
    name.first = json["first_name"] as! String
    name.last = json["last_name"] as! String
  }
}

// MARK: - Person Enum Details
extension User {
  struct UserSummary {
    var selfSummary: String
    var lifeSummary: String
    var favorites: String
  }
  
  enum RelationshipStatus {
    case single
    case dating(monogamous: Bool)
    case married(monogamous: Bool)
    case openRelationship(monogamous: Bool)
  }
  
  enum SexualOrientation {
    case stright, gay, bisexual
  }
  
  enum TypeOfBody {
    case refused, thin, overweight, average, fit, jacked, extra, curvy, fullFigured
  }
  
  enum Ethnicity {
    case asian, black, hispanic, indian, middleEastern, nativeAmerican, pacificIslander, white
    case other(String)
  }
  
  enum Smoker {
    case no, sometimes, yes
  }
  
  enum Drinker {
    case no, socially, often
  }
  
  enum Diet: String {
    case Omnivore, Vegetarian, Vegan, Kosher, Halal
  }
  
  enum EducationlStatus {
    enum Status {
      case attending(Education), droppedout(Education), completed(Education)
    }
    
    enum Education {
      case highSchool(name: String)
      case twoYearCollege(name: String)
      case University(name: String)
      case postGraduate(name: String)
    }
  }
  
  enum Religion {
    enum ReligiousType {
      case atheism(seriousness: Bool)
      case christianity(seriousness: Bool)
      case judaism(seriousness: Bool)
      case islam(seriousness: Bool)
      case hinduism(seriousness: Bool)
      case buddhism(seriousness: Bool)
      case sikh(seriousness: Bool)
      case other(String, seriousness: Bool)
    }
  }
}









